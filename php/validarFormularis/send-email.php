<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
<link rel="stylesheet" href="../../css/style.css">

<?php

//0) We declare the namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
//1) Import the library
require './PHPMailer/src/Exception.php';
require './PHPMailer/src/PHPMailer.php';
require './PHPMailer/src/SMTP.php';

session_start();
$email = $_SESSION["email"];
// MAIL FOR USERS

//3) They create an instance, with the parameter true to enable exceptions
$mail = new PHPMailer(true);
try {
   //4) SMTP server configuration
   //$mail->SMTPDebug =1;                                        //Enable verbose debug output
   $mail->isSMTP();                                            //Send using SMTP
   $mail->Host       = 'smtp-elenamontiel7e4.alwaysdata.net';        //Set the SMTP server to send through
   $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
   $mail->Username   = 'elenamontiel7e4@alwaysdata.net';           //SMTP username
   $mail->Password   = 'elenamontiel7e42021';                        //SMTP password
   $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
   $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
   //5) Mail settings
   $mail->setFrom('elenamontiel7e4@alwaysdata.net', 'Mailer');
   $mail->addAddress($email);   //Add a recipient
   //$mail->addReplyTo('elenamontiel7e4@alwaysdata.net');
   //$mail->addCC('cc@example.com');
   //$mail->addBCC('bcc@example.com');

   //6) Attachment settings
   //$mail->addAttachment('/var/tmp/file.tar.gz');               //Mola es pot adjuntar binaris
   //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');          //Optional name
   
   //6) We add the contents of the mail
   $mail->isHTML(true);                                          //Habilitar formathtml
   $mail->Subject = 'FACTURA';
   ob_start();
   include_once('./user-bill.php');
   $mail->Body    = ob_get_contents();
   $mail->AltBody = 'Contingut factura usuari';
   $mail->send();
   ob_end_clean();
   $mail = 1;
} catch (Exception $e) {
   $mail = 0;
}

// MAIL PER ADMIN

//3) They create an instance, with the parameter true to enable exceptions
$mail2 = new PHPMailer(true);
try {
   //4) SMTP server configuration
   //$mail2->SMTPDebug =1;                                        //Enable verbose debug output
   $mail2->isSMTP();                                            //Send using SMTP
   $mail2->Host       = 'smtp-elenamontiel7e4.alwaysdata.net';        //Set the SMTP server to send through
   $mail2->SMTPAuth   = true;                                   //Enable SMTP authentication
   $mail2->Username   = 'elenamontiel7e4@alwaysdata.net';           //SMTP username
   $mail2->Password   = 'elenamontiel7e42021';                        //SMTP password
   $mail2->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
   $mail2->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

   //5) Mail settings
   $mail2->setFrom('elenamontiel7e4@alwaysdata.net', 'Mailer2');
   $mail2->addAddress('elenamontiel7e4@alwaysdata.net');   //Add a recipient
   //$mail->addReplyTo('elenamontiel7e4@alwaysdata.net');
   //$mail->addCC('cc@example.com');
   //$mail->addBCC('bcc@example.com');

   //6) Attachment settings
   //$mail->addAttachment('/var/tmp/file.tar.gz');               //Mola es pot adjuntar binaris
   //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');          //Optional name

   //6) We add the contents of the mail
   $mail2->isHTML(true);                                          //Habilitar formathtml
   $mail2->Subject = 'FACTURA';
   ob_start();
   include_once('./admin-bill.php');
   $mail2->Body    = ob_get_contents();
   $mail2->AltBody = 'Contingut factura admin';
   ob_end_clean();

   $mail2->send();
   $mail2 = 1;
} catch (Exception $e) {
   $mail2 = 0;
}

include_once('../../php/model.php');
include_once('../../php/read-bbdd.php');


if(isset($_SESSION['cistella'])) {
   $cistellaObjecte = unserialize($_SESSION['cistella']);
}  
// END SESSION BLOCK

$cistellaObjecte->buidar();


 // SESSION BLOCK - PUT ON THE BEGINNING OF EACH PAGE
$_SESSION['cistella']= serialize($cistellaObjecte);
// END SESSION BLOCK
if ($mail == 1) {
   echo '<div class="alert alert-success alert-dismissible fade show">
            <a href="../../index.php"> 
               <button type="button" class="close" >&times;</button>
               </a>
            <strong>Correcte!</strong> El missatge s \'ha enviat
         </div>';
   //header( "refresh:0; url= ../../index.php" );
} else {
   echo '<div class="alert alert-danger alert-dismissible fade show">
            <a href="../../index.php"> 
               <button type="button" class="close" >&times;</button>
            </a>
            <strong>ERROR!</strong>No s\'ha pogut enviar el correu
         </div>';
}
