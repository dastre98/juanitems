<!DOCTYPE html>
<html lang="en">

<head>
    <title>FACTURA USUARI</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        *{
            color: black;
        }
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }
    td{
        font-size: 17px;
    }
    </style>
</head>


<body style="margin: 10px 30px; border: 1px solid black;">
<section style="margin: 20px;">
<table>
  <!--<img src="../../img/logo.png" alt="img_logo">-->
    <tr style="text-align: right">
        <th style="font-size: 27px; padding: 10px 20px;">FACTURA</th>
    </tr>
    <tr style="text-align: right"> 
        <th style="font-size: 27px; padding: 10px 20px;">Factura #001</th>
    </tr>
</table>

  <p style="border: 1px solid #ffbb00; background-color: #ffbb00; margin: 10px 0px; "></p>
  <section style=" display: inline-block; width: 30%; vertical-align: top">
    <table style="width: 100%; text-align: left;">
        <tr>
            <th style="font-size: 20px; padding: 15px 30px 5px 30px;">Juanitems</th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td style="font-size: 20px; padding: 5px 30px;">Carrer d'Aiguablava</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="font-size: 20px; padding: 5px 30px;"> 08033 Barcelona</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    </section>
    <section style=" display: inline-block; width: 65%; text-align: right;">
        <table>
        <tr>
        <?php
                session_start();
                $nom = $_SESSION["nom"];
                $cognoms = $_SESSION["cognoms"];
                $email = $_SESSION["email"];
                $adresa = $_SESSION["adresa"];
                $adresa2 = $_SESSION["adresa2"];
                $pais = $_SESSION["pais"];
                $provincia = $_SESSION["provincia"];
                $codi_postal = $_SESSION["codi_postal"];
                echo '<tr><th style="font-weight: normal; font-size: 18px; padding: 7px 0px;">
                    <b>Nom: </b>'. $nom. ' '. $cognoms .'</th>
                    </tr>
                <tr>
                    <th style="font-weight: normal; font-size: 18px; padding: 7px 0px;"><b>Data: </b>' . date('Y-m-d'). ', ' . date('H:i:s', time()) . '</th>
                </tr>
                <tr>
                    <th style="font-weight: normal; font-size: 18px; padding: 7px 0px;"><b>Email: </b>' . $email . '</th>
                </tr>
                <tr>
                    <th style="font-weight: normal; font-size: 18px; padding: 7px 0px;"><b>Adreça 1: </b>' .$adresa. '</th>
                </tr>
                <tr>
                    <th style="font-weight: normal; font-size: 18px; padding: 7px 0px;"><b>Adreça 2: </b>' .$adresa2. '<br><p>' .$codi_postal . ' ' .  $provincia . ' ' .  $pais . '</th> 
                </tr>';
                ?>
        </tr>
        </table>
    </section>

    <table style="width: 30%; margin: 10px 0px;">
    <tr>
        <th style="width: 100%; background-color: #FFDC00; padding: 10px; border-bottom: 5px solid #ffbb00;">MÉTODE DE PAGAMENT</th>
    </tr>
    <tr>
        <?php
            $pagament = $_SESSION["pagament"];
            echo '<td style="width: 100%; padding: 10px;">'. $pagament. '</td>';
        ?>
    </tr>
    </table>
    <?php
        // SESSION BLOCK - PUT ON THE BEGINNING OF EACH PAGE
        include_once('../../php/model.php');
        include_once('../../php/read-bbdd.php');
        

        if(isset($_SESSION['cistella'])) {
            $cistellaObjecte = unserialize($_SESSION['cistella']);
        }  
        // END SESSION BLOCK

        ?>    
    <table>
    
        <th style="background-color: #FFDC00; padding: 10px; border-bottom: 5px solid #ffbb00;">PRODUCTE</th>
        <th style="background-color: #FFDC00; border-bottom: 5px solid #ffbb00;">QUANTITAT</th>
        <th style="background-color: #FFDC00; border-bottom: 5px solid #ffbb00;">PREU UNITARI</th>
        <th style="background-color: #FFDC00; border-bottom: 5px solid #ffbb00;">IMPORT</th>
    </tr>
    <?php

        $total = $cistellaObjecte->getTotal();
        $IVA =  $total * 0.21;
        $preuFinal = $total+$IVA;
        $i = 1;

        //PRODUCT INVOICE ITERATION - PRICE, QUANTITY, VAT, TOTAL
        foreach ($cistellaObjecte->productes as $producte) {
        echo'<tr>
                <td  style="width: 70%; padding: 7px;">'. $i . " - " . $producte->titol . '</td>
                <td style="text-align: center; width: 15%; padding: 7px;">'.  $producte->quantitat . '</td>';
                echo '<td style="text-align: right; width: 20%; padding: 7px;">'. number_format($producte->preu, 2, ",", " ") .'</td>';
                if (($producte->quantitat) > 1) {
                echo '<td style="text-align: right; width: 20%; padding: 7px;">'. number_format( (($producte->quantitat) * $producte->preu), 2, ",", " ").'</td>';
                } else {
                echo '<td style="text-align: right; width: 20%; padding: 7px;">'. number_format($producte->preu, 2, ",", " ") .'</td>';
                }
            echo '</tr>';
            $i++;
        }

        echo '<tr>
        <th></th>
        <th></th>';
        if ($preuFinal == 0) {
        echo '<th style="text-align: right; padding: 10px; font-size: 18px;">SUB.TOTAL</th>
        <th style="text-align: right; padding: 10px;"> - </th>';
        } else {
        echo '<th style="text-align: right; padding: 10px; font-size: 18px;">SUB.TOTAL</th>
        <th style="text-align: right; padding: 10px; font-size: 18px;">' . number_format($total, 2, ",", " ") . '€</th>';
        }
        echo '</tr>';
        ?>
        <tr>
        <th></th>
        <th></th>
        <th style="text-align: right; padding: 10px; font-size: 18px;">TOTAL</th>
        <?php
        if ($preuFinal == 0) {
            echo '<th style="text-align: right; padding: 10px;"> - </th>';
        } else {
            echo '<th style="text-align: right; padding: 10px; font-size: 18px;">' . number_format($preuFinal, 2, ",", " ") . '€</th>';
        }
        ?>
        </tr>
    </table>
</section>


<?php

// END SESSION BLOCK - PUT AT THE END OF EACH PAGE
$_SESSION['cistella'] = serialize($cistellaObjecte);
// END OF SESSION END BLOCK

?>
</body>
</html>