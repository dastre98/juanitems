<?php

  // IMPORTEM FUNCIONS ON TENEM DEFINIDES LES REGLES DE VALIDACIÓ
  include ('./authenticate.php');

  //AGAFEM LES DADES DEL FORMULARI, SI NO HI HA DADES LES DEIXEM COM A NULL
  $nom = isset($_REQUEST['nom']) ? $_REQUEST['nom'] : null;
  $cognoms = isset($_REQUEST['cognoms']) ? $_REQUEST['cognoms'] : null;
  $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : null;
  $adresa = isset($_REQUEST['adresa']) ? $_REQUEST['adresa'] : null;
  $adresa2 = isset($_REQUEST['adresa2']) ? $_REQUEST['adresa2'] : null;
  $pais = isset($_REQUEST['pais']) ? $_REQUEST['pais'] : null;
  $provincia = isset($_REQUEST['provincia']) ? $_REQUEST['provincia'] : null;
  $codi_postal = isset($_REQUEST['codi_postal']) ? $_REQUEST['codi_postal'] : null;
  $pagament = isset($_REQUEST['pagament']) ? $_REQUEST['pagament'] : null;


  //ARRAY ASSOCIATIU ON S'EMMAGATZEMEN ELS ERRORS DEL FORMULARI
  $errores = array();

  // COMPROVA QUE EL FORMULARI SIGUI MÉTODE POST, SI NO HO ÉS CARGA EL HTML
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Valida que el camp nom tingui la llargada correcta.
    if (!validarLlargada3($nom)) {
        $errores["nom"] = 'El camp nom ha de tenir com a mínim 3 caràcters.';
    }
    // Valida que el camp cognom tingui la llargada correcta.
    if (!validarLlargada3($cognoms)) {
        $errores["cognoms"] = 'El camp cognom ha de tenir com a mínim 3 caràcters.';
    }
    // Valida que el camp email sigui correcte.
    if (!validarEmail($email)) {
        $errores["mail"] = 'El camp email és incorrecte.';
    }
    // Valida que el camp adresa tingui més de 5 caràcters.
    if (!validarLlargada5($adresa)) {
        $errores["adresa"] = 'El camp adreça és incorrecte.';
    }
    // Valida que el camp adresa2 tingui més de 5 caràcters.
    if (!validarLlargada5($adresa)) {
      $errores["adresa2"] = 'El camp adreça 2 és incorrecte.';
    }
    // Valida que el camp país sigui un dels que es troben a la matriu.
    if (!validarPais($pais)) {
      $errores["pais"] = 'El camp pais és incorrecte.';
    }
    // Valida que el camp provincia sigui un dels que es troben a la matriu.
    if (!validarProvincia($provincia)) {
      $errores["provincia"] = 'El camp provincia és incorrecte.';
    }
    // Valida que el camp codi postal tingui la llargada correcta.
    if (!validarLlargada3($codi_postal)) {
      $errores["codi_postal"] = 'El camp codi postal ha de tenir com a mínim 3 caràcters.';
    }
    // Valida que el camp pagament sigui correcte.
    if (!validarLlargada3($codi_postal)) {
      $errores["codi_postal"] = 'El camp codi postal ha de tenir com a mínim 3 caràcters.';
    }
    
    // Valida que el camp pagament sigui un dels que es troben a la matriu.
    if (!validarPagament($pagament)) {
      $errores["pagament"] = 'El camp pagament és incorrecte.';
    }
    
    // Valida que el camp nom no estigui buit.
    if (!validarNoBuit($nom)) {
        $errores["nom"] = 'El camp nom  ha de ser no buit';
    }
  }

    // PASSEM ELS PARÀMETRES DEL FORMULARI PER SESSION
    session_start();
    $_SESSION['errores'] = serialize($errores);


    $_SESSION["nom"] = $nom;
    $_SESSION["cognoms"] = $cognoms;
    $_SESSION["email"] = $email;
    $_SESSION["adresa"] = $adresa;
    $_SESSION["adresa2"] = $adresa2;
    $_SESSION["pais"] = $pais;
    $_SESSION["provincia"] = $provincia;
    $_SESSION["codi_postal"] = $codi_postal;
    $_SESSION["pagament"] = $pagament;
  
    

   //VERIFICA SI HI HA ERRORS AL FORMULARI, SI NO N'HI HA ES REDIRECCIONA A LA PÀGINA send-email.php SI N'HI HA ES TORNA AL FORMULARI
   
    if (!$errores) {
          header('Location: ./send-email.php');
          exit;
      } else {
          header('Location: ' . $_SERVER['HTTP_REFERER']);
      }
    
?>


