<?php

// CHECK IF THE LENGTH OF THE CHAIN ​​IS LARGER OR EQUAL TO 3
function validarLlargada3($lValue){
    if(strlen($lValue) >= 3){
        return true;
    }else{
       return false;
    }
 }

 // CHECK IF THE LENGTH OF THE CHAIN ​​IS LARGER OR EQUAL TO 5
 function validarLlargada5($lValue){
    if(strlen($lValue) >= 5){
        return true;
    }else{
       return false;
    }
 }

 // CHECK THAT THE SELECTED COUNTRY IS WITHIN THE MATRIX $countries
 function validarPais($lValue) {
     $countries = array('Portugal', 'Espanya', 'França');
     if(in_array($lValue, $countries)) {
         return true;
     } else {
         return false;
     }
 }

  // CHECK THAT THE SELECTED PROVINCE IS WITHIN THE MATRIX $province
  function validarProvincia($lValue) {
    $province = array('Lleida', 'Tarragona', 'Barcelona', 'Girona');
    if(in_array($lValue, $province)) {
        return true;
    } else {
        return false;
    }
}

// CHECK THAT THE PAYMENT IS WITHIN THE PAYMENT MATRIX $payment
function validarPagament($lValue) {
    $payment = array('contra reenvors', 'bitcoin');
    if(in_array($lValue, $payment)) {
        return true;
    } else {
        return false;
    }
}

 // CHECK THAT THE FIELD IS NOT EMPTY
 function validarNoBuit($valor){
    if(trim($valor) == ''){
       return false;
    }else{
       return true;
    }
 }

 // VALIDATOR BY MAIL WITH FILTER_VALIDATE_EMAIL (check itself)
 function validarEmail($valor){
    if(filter_var($valor, FILTER_VALIDATE_EMAIL) === FALSE){
       return false;
    }else{
       return true;
    }
 }
 
?>