    <?php
    require_once "connect.php";
    $perpage = 5;
    $page = $_REQUEST['page'];

    if (!isset($page)) {
    //3)I create the sql statement
    $query = "SELECT * FROM `productes`";

    //4) I execute the sentence
    $res_query = mysqli_query($conn, $query);

    $catalegObjecte = new Cataleg();
    
    //5) Result is a multidimensional array each entry is another array with the commons of the bbdd table
    if(mysqli_num_rows($res_query)>0){
        while($res = mysqli_fetch_array($res_query)){
            $producte = new Producte();
            $producte->id = $res['id'];
            $producte->titol = $res['titol'];
            $producte->categoria = $res['categoria'];
            $producte->preu = $res['preu'];
            $producte->descripció = $res['descripcio'];
            $producte->colors = [$res['color1'], $res['color2'], $res['color3'], $res['color4']];
            $producte->stoc = $res['stoc'];
            $producte->totalVendes = $res['total_vendes'];
            $producte->caracteristiques = $res['caralcteristiques'];
            $producte->especificacions = $res['especificacions'];
            $producte->instruccions = $res['instruccions'];
            $producte->addFoto($res['foto1']);
            $producte->addFoto($res['foto2']);
            $producte->addFoto($res['foto3']);
            $producte->addFoto($res['foto4']);
            $producte->addFoto($res['foto5']);
            $producte->addFoto($res['foto6']);
            $catalegObjecte->addProducte($producte);
        }
    } else {
        echo "No hi ha resultats!";
    }
} 
if ($page > 0) {
    // CONSULT PRODUCTS PER PAGE
    $res1 = ($perpage*$page)-$perpage;

    //3) I create the sql statement
    $prodPerPage = "SELECT * FROM `productes` LIMIT $res1, $perpage";

    //4) I execute the sentence
    $res_query = mysqli_query($conn, $prodPerPage);

    $catalegObjecte = new Cataleg();

    //5) Result is a multidimensional array each entry is another array with the commons of the bbdd table
    if(mysqli_num_rows($res_query)>0){
        while($res = mysqli_fetch_array($res_query)){
            $producte = new Producte();
            $producte->id = $res['id'];
            $producte->titol = $res['titol'];
            $producte->categoria = $res['categoria'];
            $producte->preu = $res['preu'];
            $producte->descripció = $res['descripcio'];
            $producte->colors = [$res['color1'], $res['color2'], $res['color3'], $res['color4']];
            $producte->stoc = $res['stoc'];
            $producte->totalVendes = $res['total_vendes'];
            $producte->caracteristiques = $res['caralcteristiques'];
            $producte->especificacions = $res['especificacions'];
            $producte->instruccions = $res['instruccions'];
            $producte->addFoto($res['foto1']);
            $producte->addFoto($res['foto2']);
            $producte->addFoto($res['foto3']);
            $producte->addFoto($res['foto4']);
            $producte->addFoto($res['foto5']);
            $producte->addFoto($res['foto6']);
            $catalegObjecte->addProducte($producte);
        }
    } else {
        echo "No hi ha resultats!";
    }

}

$cistellaObjecte = new Cistella();

// CONSULT QUANTITY OF PRODUCTS
$quantitatProd = "SELECT COUNT(*) FROM `productes`";

//4) I execute the sentence
$result = mysqli_query($conn, $quantitatProd);

//5) Result is a multidimensional array each entry is another array with the commons of the bbdd table
if (mysqli_num_rows($result) > 0) {
   while($row = mysqli_fetch_assoc($result)) {
    //var_dump($row["COUNT(*)"]);
    $totalProd = $row["COUNT(*)"];
   }
}
mysqli_close($conn);

session_start();
$_SESSION['cataleg'] = serialize($catalegObjecte);

?>