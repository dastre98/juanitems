<?php
    // SESSION BLOCK - PUT ON THE BEGINNING OF EACH PAGE
    session_start();
    include_once('model.php');
    include_once('read.php');

    if(isset($_SESSION['cistella'])) {
        $cistellaObjecte = unserialize($_SESSION['cistella']);
    }  
    // END SESSION BLOCK

    
    $lId = $_REQUEST['id'];
    
    $cistellaObjecte->deleteProducte($lId);
    

    // END OF SESSION BLOCK - PUT AT THE END OF EACH PAGE
    $_SESSION['cistella']= serialize($cistellaObjecte);
    // FI BLOC FINAL SESSIO
    
    // AUTOMATIC RETURN TO THE FORM PAGE
    header('Location: ' . $_SERVER['HTTP_REFERER']);
?>
