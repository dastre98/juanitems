<?php

class Producte
{
    public $id;
    static $contador = -1;
    public $titol;
    public $categoria;
    public $preu;
    public $descripció;
    public $colors;
    public $stoc;
    public $totalVendes;
    public $caracteristiques;
    public $especificacions;
    public $instruccions;
    public $fotos = [];
    public $quantitat = 0;

    function getId()
    {
        echo "$this->id";
    }

    function addFoto($lFoto)
    {
        array_push($this->fotos, $lFoto);
    }
}


class Cataleg
{
    public $productes = [];

    function addProducte($lProducte)
    {
        $this->productes[] = $lProducte;
    }

    function getProducteById($lId)
    {
        foreach ($this->productes as $producte) {
            if ($producte->id == $lId) {
                return $producte;
            }
        }
    }

    function llistar()
    {
        foreach ($this->productes as $producte) {
            var_dump($producte);
        }
    }
}


class Cistella
{
    public $productes = [];
    public $errorStoc = [];


    function addProducte($lProducte)
    {
        $isInArray = false;
        foreach ($this->productes as $producte) {
            if($producte->id == $lProducte->id) {
                $producte->quantitat++;
                $isInArray = true;
                break;
            }
        }

        if($isInArray == false) {
            $lProducte->quantitat++;
            array_push($this->productes, $lProducte);
        }
    }

    function deleteProducte($lId)
    {
        $isInArray = false;
        foreach ($this->productes as $key=>$producte) {
            if ($producte->id == $lId) {
                if($producte->quantitat > 1) {
                    $producte->quantitat--;
                } else if ($producte->quantitat = 1) {
                    unset($this->productes[$key]);
                } else {
                    echo "ERROR: Hi ha hagut un problema en eliminar el producte amb l'id $lId.";
                }
                $isInArray = true;
                break;
            }
        }

        if($isInArray == false) {
            echo "ERROR: El producte amb l'id $lId no s'ha trobat a la cistella.";
        }
    }

    function llistar()
    {
        foreach ($this->productes as $producte) {
            var_dump($producte);
        }
    }

    function getTotal()
    {
        $preuTotal = 0;
        foreach ($this->productes as $producte) {
            $preuTotalPerId = $producte->preu * $producte->quantitat;
            $preuTotal += $preuTotalPerId;
        }
        return $preuTotal;
    }

    function getNumProductes()
    {
        $numProductes = 0;
        foreach ($this->productes as $producte) {
            $numProductes += $producte->quantitat;
        }
        return $numProductes;
    }

    function getProducteById($lId)
    {
        foreach ($this->productes as $producte) {
            if ($producte->id == $lId) {
                return $producte;
            }
        }
    }

    function buidar()
    {
        foreach ($this->productes as $producte) {
            $producte->quantitat = 0;
        }
        unset($this->productes);
    }

    function comprovarStoc() {
        unset($this->errorStoc);
        foreach ($this->productes as $producte) {
            $lStoc = $producte->stoc - $producte->quantitat;
            if($lStoc < 0) {
                array_push($this->errorStoc, $producte);
            }
        }

        if (!$this->errorStoc) {
            return true;
        } else {
            return false;
        }
    }
}

?>
