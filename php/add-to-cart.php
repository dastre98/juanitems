<?php
    // SESSION BLOCK - PUT ON THE BEGINNING OF EACH PAGE
    session_start();
    include_once('model.php');
    include_once('read-bbdd.php');

    if(isset($_SESSION['cistella'])) {
        $cistellaObjecte = unserialize($_SESSION['cistella']);
    }  
    // END SESSION BLOCK

    
    $lId = $_REQUEST['id'];
    $lProducte = $catalegObjecte->getProducteById($lId);
    
    $cistellaObjecte->addProducte($lProducte);


    // SESSION BLOCK - PUT ON THE BEGINNING OF EACH PAGE
    $_SESSION['cistella']= serialize($cistellaObjecte);
    // END SESSION BLOCK

    
    // AUTOMATIC RETURN TO THE FORM PAGE
    header('Location: ' . $_SERVER['HTTP_REFERER']);
?>
