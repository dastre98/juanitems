/*=================== TOGGLE NAV BUTTON ===================*/

function toggleMenu() {
    var menuLateral = document.getElementById("lateral-menu");
    var toggleBtn = document.getElementsByClassName("nav__toggle");
    for (let i = 0; i < toggleBtn.length; i++) {
        toggleBtn[i].classList.toggle("active");
    }
    menuLateral.classList.toggle("active");
}

/*=================== LATERAL NAV BASKET ===================*/

function toggleCartList() {
    document.getElementById("cart-list").classList.toggle("active");
}


/*=================== SCROLL BTN APPEAR ===================*/

window.onscroll = function() { scrollFunction() };

function scrollFunction() {
    mybutton = document.getElementById("scroll-up");
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "flex";
    } else {
        mybutton.style.display = "none";
    }
}


/*=================== SCROLL-UP BTN CLICK ===================*/

function scrollUp() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}


/*=================== VALIDATE FORM ===================*/

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
    'use strict'
  
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')
  
    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }
  
          form.classList.add('was-validated')
        }, false)
      })
  })()