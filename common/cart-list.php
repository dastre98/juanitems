<!-- SHOPPING CART LIST -->
<a href="javascript:void(0)" class="cart-list__close" onclick="toggleCartList()">&times;</a>
<hr>
<ul class="cart-list__list">
<?php
//CART-LIST ITERATION (BASKET CLASS PRODUCTS)
foreach ($cistellaObjecte->productes as $producte) {
    echo'<li class="cart-list__product">
            <div class="cart-list__img">
                <img src="' . $producte->fotos[0] .'" alt="'. $producte->titol .'" width="35%">
            </div>  
                <div class="cart-list__text-wrap">
                    <p class="cart-list__title">'. $producte->titol . '</p>
                    <p class="cart-list__quantity">Unitats: '. $producte->quantitat . '</p>
                    <p class="cart-list__price">Preu: '. number_format($producte->preu, 2, ',', '.') . ' €</p>
                    <form action="./php/delete-from-cart.php" method="get">
                        <input type="hidden" name="id" value="'.$producte->id.'">
                        <input class="cart-list__btn-delete btn btn__link" type="submit" value="Eliminar">
                    </form>
                </div>
        </li>';
}
?>
</ul>
<!-- SECTION WITH PURCHASE PRICE AND 'CHECKOUT' BUTTON -->
<div class='cart-list__checkout-section'>
<div class='cart-list__price-total'>Preu total: <?php echo number_format($cistellaObjecte->getTotal(), 2, ',', '.') ?> €</div>
<a href='checkout.php' class='cart-list__btn-checkout btn btn__link'>Compra ara</a>
</div>
 <!-- NAV SECUNDARIA (en amarillo) -->