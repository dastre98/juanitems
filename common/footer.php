<?php
// END SESSION BLOCK - PUT AT THE END OF EACH PAGE
$_SESSION['cistella'] = serialize($cistellaObjecte);
// END OF SESSION END BLOCK
?>

<div class="footer">

  <div class="footer__contact-wrap">
    <div class="footer__contact">
      <div class="footer__map">
        <div class="footer__title">On trobar-nos</div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2990.3550016218783!2d2.1843365513919006!3d41.453215099771946!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4bcfdad2100a9%3A0x3c4e7db0a61a83e0!2sInstitut%20Tecnol%C3%B2gic%20de%20Barcelona!5e0!3m2!1ses!2ses!4v1637421652438!5m2!1ses!2ses" width="300" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
      </div>
    </div>

    <div class="footer__references">
      <div class="footer__title">Contacta amb nosaltres</div>
      <div class="footer__item">
        <a href="tel: 937 07 00 20" class="footer__link"><i class="fas fa-phone-volume"></i>+034 937 07 00 20</a>
      </div>
      <div class="footer__item">
        <a href="mailto:elenamontiel7e4@alwaysdata.net" class="footer__link"><i class="fas fa-envelope"></i>info@juanitems.com</a>
      </div>
    </div>

    <div class="footer__links-wrap">
      <span class="footer__title">Links</span>
      <ul class="footer__list" id="footer-list">
        <li class="footer__item">
          <a href="news.php" class="footer__link">
            <span>Noticies</span>
          </a>
        </li>
        <li class="footer__item hidden">
          <a href="construction.html" class="footer__link">
            <span>Més venuts</span>
          </a>
        </li>
        <li class="footer__item hidden">
          <a href="construction.html" class="footer__link">
            <span>Promocions</span>
          </a>
        </li>
        <li class="footer__item">
          <a href="about.php" class="footer__link">
            <span>Sobre nosaltres</span>
          </a>
        </li>
        <li class="footer__item">
          <a href="last-units.php" class="footer__link">
            <span>Ultimes unitats</span>
          </a>
        </li>
        <li class="footer__item hidden">
          <a href="contacte.php" class="footer__link">
            <span>Contacte</span>
          </a>
        </li>
      </ul>
    </div>

    <div class="footer__subscribe">
      <span class="footer__title">Subscriu-te per rebre les nostres ofertes setmanals!</span>
      <form action="#">
        <input type="text" placeholder="Direcció de correu*">
        <div class="checkbox-wrap">
          <input type="checkbox" id="#" name="#" value="">
          <p>He llegit i accepto les condicions generals i la política de privacitat.</p>
        </div>
        <input type="submit" name="" value="Enviar">
      </form>

      <div class="footer__icons">
        <a href="https://www.instagram.com/iTecBcn/"><i class="fab fa-instagram"></i></a>
        <a href="https://twitter.com/iTecBcn"><i class="fab fa-twitter"></i></a>
        <a href="https://www.youtube.com/channel/UCoaBtXoia1xR2T3Bb5FRUnQ"><i class="fab fa-youtube"></i></a>
        <a href="https://www.linkedin.com/company/itecbcn"><i class="fab fa-linkedin-in"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="footer__copyright">
  <p>Copyright © 2021-2022 · <a href="index.php" class="footer__link">Juanitems</a> · All rights reserved · Cookie policy · Privacy and Terms</p>
</div>
