<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Log In</title>
    <style>
      input.btn {
        border: none;
        background-color: #FFDC00;
      }

      input.btn:hover{
        background-color: #ffbb00;
      }

      a {
        color: #000000;
        text-decoration: none;
      }
      a.link{
        color: #FFFFFF;
        text-decoration: underline;
      }
      a:hover, a.link:hover {
        color: #ffbb00;
      }
      p {
        color: white;
        text-align: center;
      }
    </style>

    <script>
      function showPasswd() {
        var x = document.getElementById("password");
        if (x.type === "password") {
          x.type = "text";
        } else {
          x.type = "password";
        }
      }
    </script>
  </head>

  <body>

  <section class="vh-100" style="background-color: #000000;">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card shadow-2-strong" style="border-radius: 1rem;">
          <div class="card-body p-5 text-center">
            <div style="text-align: right;">
              <a href="../index.php" type="button" class="btn-close" aria-label="Close"></a>
            </div>
            <h3 class="mb-5">Sign in</h3>
            <form action="authenticate.php" method="post">
              <!-- Email -->
              <div class="form-outline mb-4">
                <input type="email" name="email" placeholder="Email" class="form-control form-control-lg" />
              </div>

              <!-- Password -->
              <div class="input-group mb-4">
              <input class="form-control px-3 py-2" type="password" id="password" name="password" placeholder="Password" value="">
              <span class="input-group-text" onclick="showPasswd()">
                <i class="far fa-eye" style="cursor: pointer"></i>
              </span>
            </div>
              <!-- Checkbox -->
              <div class="form-check d-flex justify-content-start mb-4">
                <input class="form-check-input" type="checkbox" value=""/>
                <label class="form-check-label" style="margin: 0px 0px 0px 10px;"> Remember password </label>
              </div>

              <input class="btn btn-lg" type="submit" value="LogIn">

              <div class="text-center fs-6" style="margin: 25px 0px 0px 0px;"> 
                <a href="#">Forget password?</a> 
                  or 
                <a href="#">Sign up</a> 
              </div>
          </form>
          </div>
        </div>
      </div>
    </div>
    <div>
      <p>Copyright © 2021-2022 · <a href="#" class="link">ITB</a> · All rights reserved</p>
  </div>
  </div>
  
</section>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
